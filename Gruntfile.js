module.exports = function(grunt) {
    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        secret: grunt.file.readJSON("secret.json"),
        less: {
            portfolio: {
                files: [{
                    "dist/css/main.css": "src/less/main.less",
                    "dist/css/bootstrap.css": "src/less/bootstrap/bootstrap.less",
                    "dist/css/bootstrap-theme.css": "src/less/bootstrap/theme.less"
                }]
            },
        },
        pug: {
            portfolio: {
                options: {
                    pretty: true
                },
                files: [{
                    "dist/index.html": "src/index.pug",
                }]
            }
        },
        concat: {
            portfolio: {
                src: [],
                dest: "src/js/main.js"
            }
        },
        sftp: {
            portfolio: {
                files: {
                    "./": "dist/**/*"
                },
                options: {
                    path: "/var/www/cezarykupaj.pl/",
                    createDirectories: true,
                    showProgress: true,
                    srcBasePath: "dist/",

                    host: "<%= secret.host %>",
                    username: "<%= secret.username %>",
                    password: "<%= secret.password %>",
                }
            }
        },
        copy: {
            all: {
                files: [
                    {
                        expand: true,
                        cwd: "dist/",
                        src: "**/*",
                        dest: "E:/xamppn/htdocs/portfolio/"
                    }
                ]
            },
            js: {
                files: [
                    {
                        src: "src/js/main.js",
                        dest: "dist/js/main.js"
                    },
                    {
                        expand: true,
                        cwd: "src/js/lib/",
                        src: "*",
                        dest: "dist/js/lib/"
                    }
                ]
            },
            images: {
                files: [
                    {
                        expand: true,
                        cwd: "src/images/",
                        src: "**/*.png",
                        dest: "dist/images/"
                    },
                    {
                        expand: true,
                        cwd: "src/images/",
                        src: "**/*.jpg",
                        dest: "dist/images/"
                    }
                ]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        cwd: "src/fonts/",
                        src: "**/*",
                        dest: "dist/fonts/"
                    }
                ]
            }
        },
        watch: {
            fonts: {
                files: [
                    "src/fonts/**/*"
                ],
                tasks: [ "copy:fonts", "copy:all" ]
            },
            images: {
                files: [
                    "src/images/**/*"
                ],
                tasks: [ "copy:images", "copy:all" ]
            },
            js: {
                files: [
                    "src/js/**/*.js"
                ],
                tasks: [ 'concat:portfolio', "copy:js", "copy:all" ]
            },
            less: {
                files: [
                    "src/less/*.less"
                ],
                tasks: [ "less:portfolio", "copy:all" ]
            },
            pug: {
                files: [
                    "src/**/*.pug"
                ],
                tasks: [ "pug:portfolio", "copy:all" ]
            }
        },
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            portfolio: {
                tasks: [ "watch:pug", "watch:less", "watch:js", "watch:images", "watch:fonts" ]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-pug");
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-ssh");

    grunt.registerTask('default', [
        "pug:portfolio", 'less:portfolio', "copy:js",
        "copy:images", "copy:fonts", "copy:all", "concurrent:portfolio"
    ]);

    grunt.registerTask("upload", [ "sftp:portfolio" ]);
};
